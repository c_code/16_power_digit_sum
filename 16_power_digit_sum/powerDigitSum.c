/* powerDigitSum
 *
 * Raise two to the power of 1,000 and add up the individual digits of the resulting product
 *
 * Author: Jeremy Tobac
 * Date: October 2014
 */
#include <stdio.h>
#include <stdlib.h>

#define NUM_LENGTH 499
#define INITIAL_NUM 2
#define PRINT_ARRAYS 0

/**@brief Add two numbers together that have a sum no greater than 500 digits long
 *
 * Numbers are added by starting at the end of the grid, and individually adding each number
 * Numbers are carried for as long as needed before starting the next digit
 *
 *@param int *pintNum: Number to add to the pintSum
 *@param int *pintSum: Number to add pintNum to
 */
void add_500_digit_num(int *pintNum, int *pintSum)
{
	int intNumCount = NUM_LENGTH;
	int intSumCount = NUM_LENGTH;
	int intTemp = 0;

	/* add each digit */
	for(intNumCount = NUM_LENGTH;intNumCount>=0;intNumCount--){
		intSumCount = intNumCount;
		intTemp = pintNum[intNumCount] + pintSum[intSumCount];

		pintSum[intSumCount] = intTemp % 10;
		intTemp /=10;

		/* continue to add when carrying the number */
		while(intTemp != 0){
			intSumCount--;
			intTemp += pintSum[intSumCount];
			pintSum[intSumCount] = intTemp % 10;
			intTemp /=10;
		}
	}
#if PRINT_ARRAYS
	/* print the result */
	for(intSumCount = 0; intSumCount <= NUM_LENGTH; intSumCount++){
		printf("%d", pintSum[intSumCount]);
	}
	printf("\n");
#endif
}

/**@brief Initialize an array to zero
 *
 *@param int *pintNum: Array to be set to zero
 */
void init_num(int *pintNum){
	int intI;
	for(intI = 0; intI <= NUM_LENGTH; intI++){
		pintNum[intI] = 0;
	}

	pintNum[NUM_LENGTH] = INITIAL_NUM;
}

/**@brief Print out every digit in an array
 *
 *@param int *pintNum: Array to print
 *@param int intLength: Length of the array
 */
void print_num(int *pintNum, int intLength){
	int intI;
	for(intI = 0; intI<=intLength;intI++){
		printf("%d", pintNum[intI]);
	}
	printf("\n");
}

/**@brief Set the internal values of two arrays equal to each other
 *
 *@param int *pintNum: Array of values to be set equal to pintSum array values
 *@param int *pintSum: Array to set array of pintNum values to
 */
void set_num_to_sum(int *pintNum, int *pintSum){
	int intI;

	for(intI = 0; intI<=NUM_LENGTH; intI++){
		pintNum[intI] = pintSum[intI];
	}
}

/**@brief Add up all of the individual digits in an array
 *
 *@param int *pintSum: Array to add of digits of
 *@param int intLength: Length of the array
 *
 *@ret Returns the sum of all of the individual digits of the array
 */
int sum_digits(int *pintSum, int intLength){
	int intI, intSum;
	intSum= 0;

	for(intI = 0; intI <= intLength; intI++){
		intSum+=pintSum[intI];
	}
	
	return intSum;
}

int main()
{
	int intI, intDigitSum;
	int *pintNum, *pintSum;;
	pintNum=(int*)malloc(sizeof(int)*(NUM_LENGTH + 1));
	if(pintNum == NULL){
		fprintf(stderr, "error malloc initial number\n");
		return -1;
	}
	
	pintSum=(int*)malloc(sizeof(int)*(NUM_LENGTH + 1));
	if(pintSum == NULL){
		fprintf(stderr, "error malloc initial sum\n");
		return -1;
	}

	init_num(pintNum);
	init_num(pintSum);

#if PRINT_ARRAYS
	print_num(pintNum, NUM_LENGTH);
	print_num(pintSum, NUM_LENGTH);
#endif
	for(intI=0; intI<1000;intI++){
		add_500_digit_num(pintNum, pintSum);
		set_num_to_sum(pintNum, pintSum);
	}

	intDigitSum = sum_digits(pintSum, NUM_LENGTH);

	printf("\nSum of digits of 2 ^ 1,0000 : %d\n", intDigitSum);

	free(pintNum);
	pintNum = NULL;
	if(pintNum){
		fprintf(stderr, "error freeing pintNum\n");
		return -1;
	}
	free(pintSum);
	pintSum = NULL;
	if(pintSum){
		fprintf(stderr, "error freeing pintSum\n");
		return -1;
	}

	return 0;
}
